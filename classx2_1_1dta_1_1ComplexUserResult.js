var classx2_1_1dta_1_1ComplexUserResult =
[
    [ "ComplexUserResult", "classx2_1_1dta_1_1ComplexUserResult.html#a58c7838d65be77bb29a528ff6541044e", null ],
    [ "init", "classx2_1_1dta_1_1ComplexUserResult.html#a92016ed24a27402834c8037783a0d2a1", null ],
    [ "maxPage", "classx2_1_1dta_1_1ComplexUserResult.html#ad71cf5633673d03fcef771bff451a4c7", null ],
    [ "page", "classx2_1_1dta_1_1ComplexUserResult.html#a1dbb510610398e13277d6b2cfb551289", null ],
    [ "updateKey", "classx2_1_1dta_1_1ComplexUserResult.html#af8b3a97b9a7a9c1baf2da1383bbcbf32", null ],
    [ "updateQuery", "classx2_1_1dta_1_1ComplexUserResult.html#aeded9f9d78e79482472cdc585ec9f76e", null ],
    [ "usersOfPageCacheKey", "classx2_1_1dta_1_1ComplexUserResult.html#ac34ccaef84a1f6f1eda5ae557fe7afab", null ]
];