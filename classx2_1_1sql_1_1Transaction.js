var classx2_1_1sql_1_1Transaction =
[
    [ "Transaction", "classx2_1_1sql_1_1Transaction.html#ad153690d5186339881c07207275ed442", null ],
    [ "~Transaction", "classx2_1_1sql_1_1Transaction.html#a2bf3e7b04f7695b5cb22c22b975a566a", null ],
    [ "commit", "classx2_1_1sql_1_1Transaction.html#a37368c610d08a8b8e78113158a320cbc", null ],
    [ "rollback", "classx2_1_1sql_1_1Transaction.html#a15fa3181adfb723ace9d55e3391685ce", null ],
    [ "disabled", "classx2_1_1sql_1_1Transaction.html#a6a1b1a533e6ed7010385e8444afb9f60", null ],
    [ "done", "classx2_1_1sql_1_1Transaction.html#aa728907ebc3355c7ce2099494b20116f", null ],
    [ "session", "classx2_1_1sql_1_1Transaction.html#a50fca3af9e4facf238ad24973bbbbdcb", null ]
];