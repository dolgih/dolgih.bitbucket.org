var structx2_1_1dta_1_1ph_1_1PostsResult =
[
    [ "Iterator", "structx2_1_1dta_1_1ph_1_1PostsResult.html#afb1097227ef99474cadc3bd7f93a6bec", null ],
    [ "PostsResult", "structx2_1_1dta_1_1ph_1_1PostsResult.html#ab61e00a3c6133b0f373bdc5d08f4522a", null ],
    [ "abstract", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a7a2a92e73e76abb71f4466bd4ffc4cdb", null ],
    [ "addTriggersForOneRecord", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a83e0df3ae973de92b40f7383cd290630", null ],
    [ "author", "structx2_1_1dta_1_1ph_1_1PostsResult.html#aa53991ff1f39202a9725a7c621711e40", null ],
    [ "author2", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a4a5aef32e16c39ecccd5ae8482288255", null ],
    [ "authorName", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a0dd2b3eaf1a8fd107b2a44e0b158218e", null ],
    [ "authorName2", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a069dc4177037e5a6a497c9d15a51637b", null ],
    [ "authorPostsNumber", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a5a2220fabcb18b1ab317036e7b387da2", null ],
    [ "authorProperties", "structx2_1_1dta_1_1ph_1_1PostsResult.html#ad9d0bad7a86b0d6eb84b8798d59945e8", null ],
    [ "begin", "structx2_1_1dta_1_1ph_1_1PostsResult.html#ae84143bd25a7e36846f26b0f4450445c", null ],
    [ "canEdit", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a242f661ec11302be4942feb43b523a4e", null ],
    [ "canRemove", "structx2_1_1dta_1_1ph_1_1PostsResult.html#aa3d8bb0b8082d8b54963299e7c42cbc9", null ],
    [ "content", "structx2_1_1dta_1_1ph_1_1PostsResult.html#ae28cd2d57ed21f69c568dd4f9da22d4d", null ],
    [ "created", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a143063957d7aeb73b1e1584fe0fae311", null ],
    [ "end", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a621523ea5c30a4d95d20af31ff7d785f", null ],
    [ "format", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a72d8818dccbabde0c3bb42b0377929ad", null ],
    [ "id", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a93cc504b7e16b39dc182056747b40f4a", null ],
    [ "init", "structx2_1_1dta_1_1ph_1_1PostsResult.html#af399effc537f25d0d611c56d7a4fdd59", null ],
    [ "init", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a869febc57b8929aa6db636177bc2e65c", null ],
    [ "isModified", "structx2_1_1dta_1_1ph_1_1PostsResult.html#ae39af70e69c2a4cb66af5de1b37d09ae", null ],
    [ "isStartTopic", "structx2_1_1dta_1_1ph_1_1PostsResult.html#ac2da87347baf8717e0b97664f888167f", null ],
    [ "modified", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a572f4de4539233f2de6c1393975daf75", null ],
    [ "page", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a9183a4ea5ce54e690172f222dfdf34db", null ],
    [ "postsOfTopicAllPagesCacheKey", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a843a7ff275b17eb378755b73d9ed9a60", null ],
    [ "postsOfTopicCacheKey", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a4ed83b3e5fc1c2ac0ea95f083f79cb7a", null ],
    [ "title", "structx2_1_1dta_1_1ph_1_1PostsResult.html#abc088cd81b3f3fc65276c4a064c6d9b8", null ],
    [ "topicId", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a6e3602955716e344c453e9f512e09262", null ],
    [ "updateKey", "structx2_1_1dta_1_1ph_1_1PostsResult.html#a3826014d2747a1fbdebc8f41589331b3", null ],
    [ "updateQuery", "structx2_1_1dta_1_1ph_1_1PostsResult.html#af7f28b12dd975f696746e9e8f94d3e21", null ]
];