var classx2_1_1dta_1_1NotificationsResult =
[
    [ "Iterator", "classx2_1_1dta_1_1NotificationsResult.html#a9f438043746f624bd8b5cf13b2dd038d", null ],
    [ "NotificationsResult", "classx2_1_1dta_1_1NotificationsResult.html#a36b07e32542ac7e9ecf9e1cbbaf34687", null ],
    [ "abstract", "classx2_1_1dta_1_1NotificationsResult.html#ab50a8dba153428e2dbb7dbcced6e3cb5", null ],
    [ "author", "classx2_1_1dta_1_1NotificationsResult.html#a1c6f1383b005ea99f6f8d37c63c3efdc", null ],
    [ "authorName", "classx2_1_1dta_1_1NotificationsResult.html#a9d6a42b39bbbf096458d4dcdd81aabb4", null ],
    [ "begin", "classx2_1_1dta_1_1NotificationsResult.html#a48ebb8c7b580f458cd7cf0b4db8c08fa", null ],
    [ "created", "classx2_1_1dta_1_1NotificationsResult.html#a08824fb90c1f06715995f6fb6392f88f", null ],
    [ "end", "classx2_1_1dta_1_1NotificationsResult.html#affec83951c176f3862d199bf6182e30a", null ],
    [ "init", "classx2_1_1dta_1_1NotificationsResult.html#a11dcba1d31394e58e0cdffaf2a9610fe", null ],
    [ "postId", "classx2_1_1dta_1_1NotificationsResult.html#a631353d0418b4ed8009279d1eb611b16", null ],
    [ "title", "classx2_1_1dta_1_1NotificationsResult.html#a5b968de45cb775be747b2b69c25daf60", null ],
    [ "what", "classx2_1_1dta_1_1NotificationsResult.html#adcaed8ec5b1d5500103e776a5ec937a2", null ],
    [ "isPrivateMessages", "classx2_1_1dta_1_1NotificationsResult.html#a6cd4095d7a8ce2272ae15585ba1d71b7", null ]
];