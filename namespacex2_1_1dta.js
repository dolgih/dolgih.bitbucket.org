var namespacex2_1_1dta =
[
    [ "adm", "namespacex2_1_1dta_1_1adm.html", "namespacex2_1_1dta_1_1adm" ],
    [ "ph", "namespacex2_1_1dta_1_1ph.html", "namespacex2_1_1dta_1_1ph" ],
    [ "BaseUserResult", "structx2_1_1dta_1_1BaseUserResult.html", "structx2_1_1dta_1_1BaseUserResult" ],
    [ "ComplexUserResult", "classx2_1_1dta_1_1ComplexUserResult.html", "classx2_1_1dta_1_1ComplexUserResult" ],
    [ "GroupsOfUserResult", "classx2_1_1dta_1_1GroupsOfUserResult.html", "classx2_1_1dta_1_1GroupsOfUserResult" ],
    [ "NotificationsResult", "classx2_1_1dta_1_1NotificationsResult.html", "classx2_1_1dta_1_1NotificationsResult" ],
    [ "PartAccess", "structx2_1_1dta_1_1PartAccess.html", "structx2_1_1dta_1_1PartAccess" ],
    [ "Rights", "structx2_1_1dta_1_1Rights.html", "structx2_1_1dta_1_1Rights" ],
    [ "UserResult", "classx2_1_1dta_1_1UserResult.html", "classx2_1_1dta_1_1UserResult" ],
    [ "Users2rise", "structx2_1_1dta_1_1Users2rise.html", "structx2_1_1dta_1_1Users2rise" ]
];