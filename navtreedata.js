var NAVTREE =
[
  [ "x2", "index.html", [
    [ "Phorum engine X2", "index.html", [
      [ "Введение", "index.html#intro_sec", null ],
      [ "Сайт", "index.html#site_sec", null ],
      [ "Отрицательные идентификаторы записей в базе данных", "index.html#negative_sec", [
        [ "Отрицательные идентификаторы групп.", "index.html#negative_groups_subsec", null ],
        [ "Отрицательные идентификаторы пользователей.", "index.html#negative_users_subsec", [
          [ "Псевдоидентификаторы", "index.html#pseudo_identifiers_subsubsec", null ]
        ] ],
        [ "Отрицательные идентификаторы разделов.", "index.html#negative_parts_subsec", null ],
        [ "Отрицательные идентификаторы постов.", "index.html#negative_posts_subsec", null ]
      ] ],
      [ "Особенные имена в базе данных", "index.html#spec_names_sec", [
        [ "Особенные имена и описания личных разделов.", "index.html#spec_names_private_parts_subsec", null ],
        [ "Особенные имена и почтовые ящики анонимных пользователей.", "index.html#spec_names_users_subsec", null ]
      ] ],
      [ "Ключи кэширования.", "index.html#cache_keys_sec", [
        [ "Источник главной страницы.", "index.html#home_key_subsec", null ],
        [ "Ключ кэширования групп.", "index.html#groups_cache_keys_subsec", null ],
        [ "Ключи кэширования разделов.", "index.html#parts_cache_keys_subsec", null ],
        [ "Ключи кэширования пользователей.", "index.html#users_cache_keys_subsec", null ],
        [ "Ключи кэширования групп пользователей.", "index.html#groups_of_user_cache_keys_subsec", null ],
        [ "Ключ кэширования прав доступа групп к разделам.", "index.html#group_access_cache_keys_subsec", null ],
        [ "Ключи кэширования групп забаненных в блогах.", "index.html#ban_group_cache_keys_subsec", null ],
        [ "Ключ кэширования извещений для пользователя", "index.html#notifications_of_user_cache_keys_subsec", null ],
        [ "Ключи кэширования тем.", "index.html#topics_cache_keys_subsec", null ],
        [ "Ключи кэширования постов.", "index.html#posts_cache_keys_subsec", null ],
        [ "Ключ кэширования списка последних постов.", "index.html#lastPosts_cache_keys_subsec", null ],
        [ "Список ключей кэширования", "index.html#cache_keys_subsec", null ]
      ] ],
      [ "Авторские разделы (блоги)", "index.html#blogs_sec", [
        [ "Права автора", "index.html#blogs_author_rights_subsec", null ]
      ] ]
    ] ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Variables", "globals_vars.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"actions_8cpp.html",
"classx2_1_1Page.html#aa172875c5716e257b31c275fc8706b00",
"classx2_1_1dta_1_1NotificationsResult.html#a5b968de45cb775be747b2b69c25daf60",
"classx2_1_1sql_1_1Result.html#afb39076a238c4d514567cefbf66c7e05",
"functions_func_o.html",
"structx2_1_1Cfg_1_1Admin.html",
"structx2_1_1Number.html#aa6192f64d42b00fad5cf97eee66fbeeb",
"structx2_1_1Visited_1_1Topic.html#a61013ddbebfaa1c4882143b134ac9608",
"structx2_1_1dta_1_1ph_1_1TopicsResult.html#aaa9ffc52ab91cd88c3919ddfa8dfa7bf"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';