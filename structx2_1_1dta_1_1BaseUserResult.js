var structx2_1_1dta_1_1BaseUserResult =
[
    [ "Iterator", "structx2_1_1dta_1_1BaseUserResult.html#ad6324281a4fd597455223cf8a4404309", null ],
    [ "begin", "structx2_1_1dta_1_1BaseUserResult.html#a63abbf3873ec79d8f6ef93108f8b5f52", null ],
    [ "created", "structx2_1_1dta_1_1BaseUserResult.html#ae24be8a283dbfcebc638b5efd7be7307", null ],
    [ "email", "structx2_1_1dta_1_1BaseUserResult.html#a7f30bc1514f62db72cffa42f700e681f", null ],
    [ "end", "structx2_1_1dta_1_1BaseUserResult.html#ab9e70003c9c996a6aeb4b302105b2140", null ],
    [ "getProperties", "structx2_1_1dta_1_1BaseUserResult.html#ab744da4f19aa939f3b145817cbc007e3", null ],
    [ "id", "structx2_1_1dta_1_1BaseUserResult.html#ad4ddb5ef3bcd07bdf4c51432e43f1064", null ],
    [ "lastActivity", "structx2_1_1dta_1_1BaseUserResult.html#ab043472066da963dd65c63055ee63d75", null ],
    [ "modified", "structx2_1_1dta_1_1BaseUserResult.html#aab8f55361e08b03c401713c2726d4a12", null ],
    [ "name", "structx2_1_1dta_1_1BaseUserResult.html#a240e44facae84503d4f423f0b0823b7a", null ],
    [ "password", "structx2_1_1dta_1_1BaseUserResult.html#a552fc1c2d57937368a2231e41a69d98f", null ],
    [ "phone", "structx2_1_1dta_1_1BaseUserResult.html#ace72b5f454ae2f45a421e28b78c52dc8", null ],
    [ "postsNumber", "structx2_1_1dta_1_1BaseUserResult.html#af5f5a1e91dd65845dccb351b3d5d2bb8", null ],
    [ "properties", "structx2_1_1dta_1_1BaseUserResult.html#adbbfa0f213d14dbb3db8007c12c7c05a", null ],
    [ "allProperties", "structx2_1_1dta_1_1BaseUserResult.html#ab2741bf01a12663afad364b335f64244", null ]
];