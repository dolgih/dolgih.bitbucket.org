var structx2_1_1Navigation =
[
    [ "Navigation", "structx2_1_1Navigation.html#a1ad7027ff1b8e1048eadd09f137f9889", null ],
    [ "canLeft", "structx2_1_1Navigation.html#a78e1c19e899b1a0acc8e99819539058a", null ],
    [ "canLeftOrRight", "structx2_1_1Navigation.html#a42c35026e05493153afc7f0631e25397", null ],
    [ "canRight", "structx2_1_1Navigation.html#a8f336cd165eaa65d57f54f012ea92a1a", null ],
    [ "leftPage", "structx2_1_1Navigation.html#a5ba1885630ee8de3dc5a635514bedb4d", null ],
    [ "render", "structx2_1_1Navigation.html#ace82ab21c25850e0fc34c448b66f5780", null ],
    [ "rightPage", "structx2_1_1Navigation.html#aa89f956f13c96ea230ce2822e256da32", null ],
    [ "tr", "structx2_1_1Navigation.html#a574c5168036cd122b1d93015f0b30afe", null ],
    [ "urlForPage", "structx2_1_1Navigation.html#ac7405b8e9390431450247d622e4508c2", null ],
    [ "currPage", "structx2_1_1Navigation.html#aaaaf9bc8f8bbe4b20e07510215bac080", null ],
    [ "maxPage", "structx2_1_1Navigation.html#a471e1bbc5ab8c6a563325a132f05bcba", null ],
    [ "out", "structx2_1_1Navigation.html#a326299c6c9cdb79583de486fd1a4e81d", null ],
    [ "p", "structx2_1_1Navigation.html#af05730a43cafbaf178e46f3292a0111d", null ],
    [ "urlForPageFormat", "structx2_1_1Navigation.html#a741a154d698f66a4f514c1729f690abc", null ]
];