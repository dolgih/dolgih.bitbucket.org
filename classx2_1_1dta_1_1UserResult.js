var classx2_1_1dta_1_1UserResult =
[
    [ "ErrorNotFound", "structx2_1_1dta_1_1UserResult_1_1ErrorNotFound.html", "structx2_1_1dta_1_1UserResult_1_1ErrorNotFound" ],
    [ "UserResult", "classx2_1_1dta_1_1UserResult.html#a258d4c57ccfa93f8f3fd12d9768a619a", null ],
    [ "UserResult", "classx2_1_1dta_1_1UserResult.html#ab2ad5548d86072b90ec31fa05db3fd25", null ],
    [ "findBy", "classx2_1_1dta_1_1UserResult.html#a109cd203e86bebb9c54f1cb41f3790b6", null ],
    [ "findById", "classx2_1_1dta_1_1UserResult.html#af1d2ec5beeadb780f9e7557356e40891", null ],
    [ "insert", "classx2_1_1dta_1_1UserResult.html#a97bbe2db9be766ddd9eebd7e113c8f2b", null ],
    [ "update", "classx2_1_1dta_1_1UserResult.html#a8adc93e30b6ef591ffdf81f9a968535e", null ]
];