var namespacex2_1_1sql =
[
    [ "BadValueCastError", "structx2_1_1sql_1_1BadValueCastError.html", "structx2_1_1sql_1_1BadValueCastError" ],
    [ "CachedResult", "classx2_1_1sql_1_1CachedResult.html", "classx2_1_1sql_1_1CachedResult" ],
    [ "ComplexCachedResult", "classx2_1_1sql_1_1ComplexCachedResult.html", "classx2_1_1sql_1_1ComplexCachedResult" ],
    [ "ConnectionError", "structx2_1_1sql_1_1ConnectionError.html", "structx2_1_1sql_1_1ConnectionError" ],
    [ "Error", "structx2_1_1sql_1_1Error.html", "structx2_1_1sql_1_1Error" ],
    [ "ErrorIntegrityConstraintViolation", "structx2_1_1sql_1_1ErrorIntegrityConstraintViolation.html", "structx2_1_1sql_1_1ErrorIntegrityConstraintViolation" ],
    [ "ErrorLockNotAvailable", "structx2_1_1sql_1_1ErrorLockNotAvailable.html", "structx2_1_1sql_1_1ErrorLockNotAvailable" ],
    [ "Iterator", "classx2_1_1sql_1_1Iterator.html", "classx2_1_1sql_1_1Iterator" ],
    [ "noncopyable", "classx2_1_1sql_1_1noncopyable.html", "classx2_1_1sql_1_1noncopyable" ],
    [ "PhSession", "classx2_1_1sql_1_1PhSession.html", "classx2_1_1sql_1_1PhSession" ],
    [ "Result", "classx2_1_1sql_1_1Result.html", "classx2_1_1sql_1_1Result" ],
    [ "Session", "classx2_1_1sql_1_1Session.html", "classx2_1_1sql_1_1Session" ],
    [ "Statement", "classx2_1_1sql_1_1Statement.html", "classx2_1_1sql_1_1Statement" ],
    [ "Transaction", "classx2_1_1sql_1_1Transaction.html", "classx2_1_1sql_1_1Transaction" ]
];