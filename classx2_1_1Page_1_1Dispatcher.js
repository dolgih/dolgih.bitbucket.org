var classx2_1_1Page_1_1Dispatcher =
[
    [ "Dispatcher", "classx2_1_1Page_1_1Dispatcher.html#a1ff709f4f1cbf181a9cd04e490e1b4ea", null ],
    [ "~Dispatcher", "classx2_1_1Page_1_1Dispatcher.html#ab9e77b26f3dd16f14ed5967bca2baabb", null ],
    [ "findDemiurge", "classx2_1_1Page_1_1Dispatcher.html#ae524210d267bc6555c5a92bbdd73e4fb", null ],
    [ "init", "classx2_1_1Page_1_1Dispatcher.html#a2941442fe0099a97606974cc2e7b5fc8", null ],
    [ "url", "classx2_1_1Page_1_1Dispatcher.html#a6fe8dba1b0c2afe349b6a70e44201620", null ],
    [ "children", "classx2_1_1Page_1_1Dispatcher.html#a83984a676e4b3521143736b115decc38", null ],
    [ "key", "classx2_1_1Page_1_1Dispatcher.html#aee87ac854736170302c715d88deb8091", null ],
    [ "map", "classx2_1_1Page_1_1Dispatcher.html#a001899ac4b4598d99be7b1bbd90b301d", null ],
    [ "parent", "classx2_1_1Page_1_1Dispatcher.html#a9f53c0a8474beb80d9ecd5e11931eb91", null ],
    [ "theUrl", "classx2_1_1Page_1_1Dispatcher.html#acc0a19a22dc42cab1c7c52000e8abbbc", null ]
];