var structx2_1_1MenuItem =
[
    [ "IdComp", "structx2_1_1MenuItem_1_1IdComp.html", "structx2_1_1MenuItem_1_1IdComp" ],
    [ "Menu", "structx2_1_1MenuItem.html#afac8cf6470a63641a449d6e12871ccf8", null ],
    [ "Property", "structx2_1_1MenuItem.html#abd50b7947f22113bef8bb8c2804a0932", [
      [ "Hidden", "structx2_1_1MenuItem.html#abd50b7947f22113bef8bb8c2804a0932af5a062ac515be4693fb34448f0a530df", null ],
      [ "Disabled", "structx2_1_1MenuItem.html#abd50b7947f22113bef8bb8c2804a0932a02a2818627ac2c4ef6a61c7e3823a9d3", null ]
    ] ],
    [ "Type", "structx2_1_1MenuItem.html#a24821ed503df77cfab16a8e2f0314a1b", [
      [ "Simple", "structx2_1_1MenuItem.html#a24821ed503df77cfab16a8e2f0314a1bafce2e1a592c334e8f968d57249a64b1a", null ],
      [ "SubMenu", "structx2_1_1MenuItem.html#a24821ed503df77cfab16a8e2f0314a1ba3a8c1c13cb3c32bda8d377d340b79096", null ],
      [ "Separator", "structx2_1_1MenuItem.html#a24821ed503df77cfab16a8e2f0314a1ba53e05e03dcf6a2bc0faec23e935eaf34", null ]
    ] ],
    [ "MenuItem", "structx2_1_1MenuItem.html#aa49dd4d75660de3ca68b213fcdb05de9", null ],
    [ "MenuItem", "structx2_1_1MenuItem.html#a276990ed3dbced86425ad1ed7c4fc9b3", null ],
    [ "MenuItem", "structx2_1_1MenuItem.html#a343047bd5279be44ce5c9ad1d85d3910", null ],
    [ "isDisabled", "structx2_1_1MenuItem.html#a935d8794173abaa083cc49c2f6b5b42a", null ],
    [ "isHidden", "structx2_1_1MenuItem.html#a2b929032bdddf712daa15e08f571d6f1", null ],
    [ "isSeparator", "structx2_1_1MenuItem.html#adc4e058ec1e2880323a0e0ae980c373f", null ],
    [ "isSimple", "structx2_1_1MenuItem.html#acd59fad0cdeeb238200be3d2580d7b5f", null ],
    [ "isSubMenu", "structx2_1_1MenuItem.html#a2ad054072e0d25d4f7b478983a956acc", null ],
    [ "id", "structx2_1_1MenuItem.html#afc6a6f789d3d48dfa9496ba3c73bb5c0", null ],
    [ "link", "structx2_1_1MenuItem.html#a64c767606bf938b161dee278a9beb987", null ],
    [ "name", "structx2_1_1MenuItem.html#a5a86e6224f9d5937c175429576deacbb", null ],
    [ "properties", "structx2_1_1MenuItem.html#a8f439aa675f4b6af85bcdb9fc3ec6018", null ],
    [ "subMenu", "structx2_1_1MenuItem.html#a92b46b8b4e4f2b48911ac7c1ff3901f3", null ],
    [ "title", "structx2_1_1MenuItem.html#aded3e01cd9c04bc580ce43ea3a10275d", null ]
];