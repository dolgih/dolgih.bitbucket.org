var dir_a642b49fd614e67de34cbc9c2a4694da =
[
    [ "adm", "dir_4fd701c85c54996e41fd539468439054.html", "dir_4fd701c85c54996e41fd539468439054" ],
    [ "ph", "dir_1108091efff1b0c33c8f423ee68e89c6.html", "dir_1108091efff1b0c33c8f423ee68e89c6" ],
    [ "login.cpp", "ctl_2login_8cpp.html", null ],
    [ "notifications.cpp", "ctl_2notifications_8cpp.html", null ],
    [ "rmUser.cpp", "rmUser_8cpp.html", null ],
    [ "search.cpp", "ctl_2search_8cpp.html", null ],
    [ "user.cpp", "ctl_2user_8cpp.html", null ],
    [ "visited.cpp", "visited_8cpp.html", "visited_8cpp" ],
    [ "visited.h", "visited_8h.html", [
      [ "Visited", "classx2_1_1Visited.html", "classx2_1_1Visited" ],
      [ "Cache", "structx2_1_1Visited_1_1Cache.html", "structx2_1_1Visited_1_1Cache" ],
      [ "Stat", "structx2_1_1Visited_1_1Cache_1_1Stat.html", "structx2_1_1Visited_1_1Cache_1_1Stat" ],
      [ "CacheElement", "structx2_1_1Visited_1_1CacheElement.html", "structx2_1_1Visited_1_1CacheElement" ],
      [ "Topic", "structx2_1_1Visited_1_1Topic.html", "structx2_1_1Visited_1_1Topic" ],
      [ "User", "structx2_1_1Visited_1_1User.html", "structx2_1_1Visited_1_1User" ]
    ] ]
];