var dir_cfb2ffb2869bbb27b7281952022eb856 =
[
    [ "cfg.cpp", "cfg_8cpp.html", "cfg_8cpp" ],
    [ "cfg.h", "cfg_8h.html", [
      [ "Cfg", "classx2_1_1Cfg.html", "classx2_1_1Cfg" ],
      [ "Postgres", "structx2_1_1Cfg_1_1Postgres.html", "structx2_1_1Cfg_1_1Postgres" ],
      [ "Logging", "structx2_1_1Cfg_1_1Logging.html", "structx2_1_1Cfg_1_1Logging" ],
      [ "Localization", "structx2_1_1Cfg_1_1Localization.html", "structx2_1_1Cfg_1_1Localization" ],
      [ "Themes", "structx2_1_1Cfg_1_1Themes.html", "structx2_1_1Cfg_1_1Themes" ],
      [ "Theme", "structx2_1_1Cfg_1_1Themes_1_1Theme.html", "structx2_1_1Cfg_1_1Themes_1_1Theme" ],
      [ "Smtp", "structx2_1_1Cfg_1_1Smtp.html", "structx2_1_1Cfg_1_1Smtp" ],
      [ "Admin", "structx2_1_1Cfg_1_1Admin.html", "structx2_1_1Cfg_1_1Admin" ],
      [ "Policy", "structx2_1_1Cfg_1_1Admin_1_1Policy.html", "structx2_1_1Cfg_1_1Admin_1_1Policy" ],
      [ "NewUser", "structx2_1_1Cfg_1_1Admin_1_1Policy_1_1NewUser.html", "structx2_1_1Cfg_1_1Admin_1_1Policy_1_1NewUser" ],
      [ "Sql", "structx2_1_1Cfg_1_1Admin_1_1Sql.html", "structx2_1_1Cfg_1_1Admin_1_1Sql" ],
      [ "Cookie", "structx2_1_1Cfg_1_1Cookie.html", "structx2_1_1Cfg_1_1Cookie" ],
      [ "Client", "structx2_1_1Cfg_1_1Client.html", "structx2_1_1Cfg_1_1Client" ],
      [ "Server", "structx2_1_1Cfg_1_1Server.html", "structx2_1_1Cfg_1_1Server" ],
      [ "Message", "structx2_1_1Cfg_1_1Message.html", "structx2_1_1Cfg_1_1Message" ],
      [ "MaxSize", "structx2_1_1Cfg_1_1Message_1_1MaxSize.html", "structx2_1_1Cfg_1_1Message_1_1MaxSize" ],
      [ "LastPosts", "structx2_1_1Cfg_1_1LastPosts.html", "structx2_1_1Cfg_1_1LastPosts" ],
      [ "Avatar", "structx2_1_1Cfg_1_1Avatar.html", "structx2_1_1Cfg_1_1Avatar" ],
      [ "Table", "structx2_1_1Cfg_1_1Table.html", "structx2_1_1Cfg_1_1Table" ],
      [ "Cache", "structx2_1_1Cfg_1_1Cache.html", "structx2_1_1Cfg_1_1Cache" ],
      [ "Fill", "structx2_1_1Cfg_1_1Cache_1_1Fill.html", "structx2_1_1Cfg_1_1Cache_1_1Fill" ],
      [ "Try", "structx2_1_1Cfg_1_1Cache_1_1Fill_1_1Try.html", "structx2_1_1Cfg_1_1Cache_1_1Fill_1_1Try" ],
      [ "Activity", "structx2_1_1Cfg_1_1Activity.html", "structx2_1_1Cfg_1_1Activity" ],
      [ "Xss", "structx2_1_1Cfg_1_1Xss.html", "structx2_1_1Cfg_1_1Xss" ],
      [ "MessagesCatalog", "structx2_1_1MessagesCatalog.html", "structx2_1_1MessagesCatalog" ]
    ] ]
];