var structx2_1_1User =
[
    [ "User", "structx2_1_1User.html#adc962edc1c8cdb2f14b88e22ea12b0ae", null ],
    [ "bannedEverywhere", "structx2_1_1User.html#a1b86e9254956762d03d2d030fe4bbafa", null ],
    [ "canCorrespondence", "structx2_1_1User.html#acd932bf48b7391379b9011aaebd3a0d4", null ],
    [ "createAnonymous", "structx2_1_1User.html#a1d1fd1e8d2c7fb081b2a08c8d7e3b177", null ],
    [ "hasAnonymousId", "structx2_1_1User.html#af76d0a4f37c9f6904de106bc6bc00428", null ],
    [ "hasNotification", "structx2_1_1User.html#a7bc1f2a804c6fad23f6c80f75fa66082", null ],
    [ "init", "structx2_1_1User.html#a45c6836113fe2395b187f3f28086d0b3", null ],
    [ "isMemberOfGroup", "structx2_1_1User.html#ab02b0bc845ff1b43916c1af3e7574997", null ],
    [ "locale", "structx2_1_1User.html#a5fd4a46d9fe6847bf4bdc459d29d9a86", null ],
    [ "locale", "structx2_1_1User.html#ae70cf0139a389f65e0aeefc27feefebb", null ],
    [ "privatePart", "structx2_1_1User.html#abd624e36419c1ee9faac68c6f976a8c6", null ],
    [ "groupsOfUser", "structx2_1_1User.html#a03db3910e9bdd263778f422445ad7613", null ],
    [ "hasAnswers", "structx2_1_1User.html#af63f58b6758a3f94ac6aa5c163837979", null ],
    [ "hasMessages", "structx2_1_1User.html#aea599236778b3f32199b19fdeb9eb6ae", null ],
    [ "id", "structx2_1_1User.html#a2cb01fd5e4e68a2a145399a7585766c9", null ],
    [ "isAdmin", "structx2_1_1User.html#a204f5145f1bab4e6b39322ac5f08e4ec", null ],
    [ "isAnonymous", "structx2_1_1User.html#af39e62eb28891b0ebb4f5c3509163a2d", null ],
    [ "isModerator", "structx2_1_1User.html#ab10d450d01bf15ae65dc51279c027928", null ],
    [ "name", "structx2_1_1User.html#adfb7adfb8302d159b1161f2f64f29547", null ],
    [ "notifications", "structx2_1_1User.html#a81db5429885096865a5d48a32cb65702", null ],
    [ "theLocale", "structx2_1_1User.html#a78f4369330049045478e04cd3921f3d4", null ],
    [ "theme", "structx2_1_1User.html#a6bba86863b91b5f5d3e1798079a1154d", null ]
];