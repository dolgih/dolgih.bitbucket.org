var structx2_1_1bbcode_1_1Descriptor =
[
    [ "ValueVector", "structx2_1_1bbcode_1_1Descriptor.html#a17d84e07ee0bd5b36f25ce910a4dcfee", null ],
    [ "Category", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0", [
      [ "categoryNone", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0a2462b31d6d5ed9951a26ac362ec01936", null ],
      [ "categoryFontStyle", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0aee96ad030c73e2c9c12c021cbd52a7be", null ],
      [ "categorySubSuperScript", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0a128864f4f10fa3e7113e60a42328be7e", null ],
      [ "categoryFontColorSize", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0ac174ace0cf82a4c7380a3a14bac46f08", null ],
      [ "categoryUrl", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0ac1c78d8423ff4b9aa3cb6055ce53740a", null ],
      [ "categoryLine", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0af9b7effa2ce9d23a4e93243f729b2560", null ],
      [ "categoryList", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0a343b92d912769da10940406b1a8891cd", null ],
      [ "categoryInsertion", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0abb3575bc2208df4b36fc506a81e56fef", null ],
      [ "categoryAlign", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0aac9d20e7c0d5125f60565e65e953bc09", null ],
      [ "categoryAnother", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0a5f65b9111e9d6d93b3b500e41f115721", null ],
      [ "categoryBound", "structx2_1_1bbcode_1_1Descriptor.html#abafb5c97d44d9ec83c176a4c338487c0a869cc29f91b94c469dcb0855e1ee3621", null ]
    ] ],
    [ "Properties", "structx2_1_1bbcode_1_1Descriptor.html#af7f2ce10d6cbcdff30daced269a759c6", [
      [ "propertiesNone", "structx2_1_1bbcode_1_1Descriptor.html#af7f2ce10d6cbcdff30daced269a759c6ad37610f1013882f8bd4ba272729f477e", null ],
      [ "single", "structx2_1_1bbcode_1_1Descriptor.html#af7f2ce10d6cbcdff30daced269a759c6ad589f469dbf3f1883e94c7af31460272", null ],
      [ "simple", "structx2_1_1bbcode_1_1Descriptor.html#af7f2ce10d6cbcdff30daced269a759c6ae5f9441a5a7a13bc18db24216af8d1d0", null ],
      [ "mustHaveValue", "structx2_1_1bbcode_1_1Descriptor.html#af7f2ce10d6cbcdff30daced269a759c6ae8e11a41b8a9af47178c5034d2945506", null ],
      [ "mayHaveValue", "structx2_1_1bbcode_1_1Descriptor.html#af7f2ce10d6cbcdff30daced269a759c6a019eb5489a098ade805b45970e066818", null ],
      [ "mayHaveParameters", "structx2_1_1bbcode_1_1Descriptor.html#af7f2ce10d6cbcdff30daced269a759c6a720a84658c8bbb2a2da7f84dce2f7eab", null ]
    ] ],
    [ "Descriptor", "structx2_1_1bbcode_1_1Descriptor.html#aecd94bc5ff8aaebd33d58cac64437471", null ],
    [ "~Descriptor", "structx2_1_1bbcode_1_1Descriptor.html#a3c6c17a5d71647ee795d0bb7a2f44e3a", null ],
    [ "id", "structx2_1_1bbcode_1_1Descriptor.html#a3647c250120a36648f0fc3d1e3dd499e", null ],
    [ "init", "structx2_1_1bbcode_1_1Descriptor.html#a84263552dcc4675aff8d7ddb1e6c4ecc", null ],
    [ "insertion", "structx2_1_1bbcode_1_1Descriptor.html#a1ee4b858a306a928c1fe3d1e61e19ae1", null ],
    [ "loadParams", "structx2_1_1bbcode_1_1Descriptor.html#a7610436b01908e52f8d5b5f7bb71676c", null ],
    [ "loadValue", "structx2_1_1bbcode_1_1Descriptor.html#a63f1077d92e4da26f43c600f3509019e", null ],
    [ "renderClose", "structx2_1_1bbcode_1_1Descriptor.html#a5daef2a456ff79d272fc4e2822c08612", null ],
    [ "renderOpen", "structx2_1_1bbcode_1_1Descriptor.html#a42752e678bf7e5fc49cc343eb3a6f04c", null ],
    [ "setData", "structx2_1_1bbcode_1_1Descriptor.html#ad5b7f65918002948e024389f3322f757", null ],
    [ "values", "structx2_1_1bbcode_1_1Descriptor.html#aaf6a5547a6fdb8aac01dbae139eac855", null ],
    [ "category", "structx2_1_1bbcode_1_1Descriptor.html#a188c8e3efa8c14e4dc5583a736932e85", null ],
    [ "htmlTag", "structx2_1_1bbcode_1_1Descriptor.html#a243829b5dae42670852fd8f7430bc2b7", null ],
    [ "name", "structx2_1_1bbcode_1_1Descriptor.html#af011739234665b733a718156df5d5b78", null ],
    [ "properties", "structx2_1_1bbcode_1_1Descriptor.html#aaf66cd08d4bfe0fa01eee1bd7c9ba3f3", null ],
    [ "title", "structx2_1_1bbcode_1_1Descriptor.html#a248840a2a3cac5b11691ed9b923888a0", null ]
];