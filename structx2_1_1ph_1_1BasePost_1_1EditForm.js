var structx2_1_1ph_1_1BasePost_1_1EditForm =
[
    [ "EditForm", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#a7d3508666d6908153ae1894c45cb18ab", null ],
    [ "setAutofocus", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#ac95f22de741d8ffa94d6429606bcef66", null ],
    [ "setTitle", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#abbe7ee85b1001836ec499d3014ffb996", null ],
    [ "validate", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#aa919845242a90634ce430714003b7fef", null ],
    [ "abstract", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#af27c562c01a23d2349b0d25a1d68518f", null ],
    [ "author2", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#acdedf5e2aaa495937d2a4dab2a0bc28e", null ],
    [ "content", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#ac4db4aee31fc633bab3805b101d45e40", null ],
    [ "format", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#a8db6ba416087f7145bbb69d707aabde9", null ],
    [ "pinned", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#a51fa907a3e417bfb6fa8901769073936", null ],
    [ "preview", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#a330ae5178650441619a79bbb453512c0", null ],
    [ "setAsHome", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#a520bb62f606f123090ee7088125693b3", null ],
    [ "submit", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#ab50629f62fde1f194950cb9b1c230a9c", null ],
    [ "title", "structx2_1_1ph_1_1BasePost_1_1EditForm.html#aed71cf76ec296311d9801a480d2f33e2", null ]
];