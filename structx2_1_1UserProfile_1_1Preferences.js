var structx2_1_1UserProfile_1_1Preferences =
[
    [ "Preferences", "structx2_1_1UserProfile_1_1Preferences.html#a49af6d474204e53f29bec9dcee88ce47", null ],
    [ "checkAuthorizationAll", "structx2_1_1UserProfile_1_1Preferences.html#af17281d804ca83c0bec0f175f4ec4f07", null ],
    [ "answers", "structx2_1_1UserProfile_1_1Preferences.html#a1d5b934420a71fe871755f7d5b457166", null ],
    [ "lAnswers", "structx2_1_1UserProfile_1_1Preferences.html#a3e12f3f7a356e382bafbaff230fb56e5", null ],
    [ "lLocale", "structx2_1_1UserProfile_1_1Preferences.html#a18102b0c9a59bc29234c6495c2592cea", null ],
    [ "locale", "structx2_1_1UserProfile_1_1Preferences.html#a74348a23fe64cb599ea0a67121e630a3", null ],
    [ "lQuotes", "structx2_1_1UserProfile_1_1Preferences.html#ac15ca6e00d89690752e4a157b4c3c480", null ],
    [ "lTheme", "structx2_1_1UserProfile_1_1Preferences.html#ac11a4cca71d4acee5367bf643610e8d2", null ],
    [ "quotes", "structx2_1_1UserProfile_1_1Preferences.html#ac78c6c1e8fd83ee393ccd7b8160e9859", null ],
    [ "theme", "structx2_1_1UserProfile_1_1Preferences.html#a5ca42339ef4ecf3ce91803c163ab998b", null ]
];