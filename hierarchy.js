var hierarchy =
[
    [ "x2::Visited::Cache< x2::Visited::Topic >", "structx2_1_1Visited_1_1Cache.html", null ],
    [ "x2::Visited::Cache< x2::Visited::User >", "structx2_1_1Visited_1_1Cache.html", null ],
    [ "x2::ChildOf< Form, Input >", "structx2_1_1ChildOf.html", [
      [ "x2::Input", "structx2_1_1Input.html", [
        [ "x2::Button", "structx2_1_1Button.html", null ],
        [ "x2::Email", "structx2_1_1Email.html", null ],
        [ "x2::File", "structx2_1_1File.html", null ],
        [ "x2::GroupedInputs", "structx2_1_1GroupedInputs.html", [
          [ "x2::MultiCheckBox", "structx2_1_1MultiCheckBox.html", null ],
          [ "x2::UserProfile::Main::Banned", "structx2_1_1UserProfile_1_1Main_1_1Banned.html", null ]
        ] ],
        [ "x2::Hidden", "structx2_1_1Hidden.html", null ],
        [ "x2::Number", "structx2_1_1Number.html", null ],
        [ "x2::Password", "structx2_1_1Password.html", null ],
        [ "x2::Select", "structx2_1_1Select.html", null ],
        [ "x2::Submit", "structx2_1_1Submit.html", [
          [ "x2::CheckBox", "structx2_1_1CheckBox.html", null ]
        ] ],
        [ "x2::Text", "structx2_1_1Text.html", [
          [ "x2::Regex", "structx2_1_1Regex.html", [
            [ "x2::UserProfile::Main::WebSiteField", "structx2_1_1UserProfile_1_1Main_1_1WebSiteField.html", null ]
          ] ]
        ] ],
        [ "x2::TextArea", "structx2_1_1TextArea.html", null ],
        [ "x2::UrlField", "structx2_1_1UrlField.html", null ]
      ] ]
    ] ],
    [ "x2::ChildOf< P, Self >", "structx2_1_1ChildOf.html", [
      [ "x2::ChildAndParentOf< P, C, Self >", "structx2_1_1ChildAndParentOf.html", null ]
    ] ],
    [ "x2::ChildOf< Page, Form >", "structx2_1_1ChildOf.html", [
      [ "x2::ChildAndParentOf< Page, Input, Form >", "structx2_1_1ChildAndParentOf.html", [
        [ "x2::Form", "structx2_1_1Form.html", [
          [ "x2::adm::Groups::EditForm", "structx2_1_1adm_1_1Groups_1_1EditForm.html", null ],
          [ "x2::adm::Part::EditForm", "structx2_1_1adm_1_1Part_1_1EditForm.html", null ],
          [ "x2::adm::Sql::QueryForm", "structx2_1_1adm_1_1Sql_1_1QueryForm.html", null ],
          [ "x2::adm::Sys::TheForm", "structx2_1_1adm_1_1Sys_1_1TheForm.html", null ],
          [ "x2::adm::Xss::TheForm", "structx2_1_1adm_1_1Xss_1_1TheForm.html", null ],
          [ "x2::Login::TheForm", "structx2_1_1Login_1_1TheForm.html", null ],
          [ "x2::Notifications::TheForm", "structx2_1_1Notifications_1_1TheForm.html", null ],
          [ "x2::ph::BasePost::EditForm", "structx2_1_1ph_1_1BasePost_1_1EditForm.html", null ],
          [ "x2::Search::TheForm", "structx2_1_1Search_1_1TheForm.html", null ],
          [ "x2::UserProfile::BaseForm", "structx2_1_1UserProfile_1_1BaseForm.html", [
            [ "x2::UserProfile::Actions", "structx2_1_1UserProfile_1_1Actions.html", null ],
            [ "x2::UserProfile::Main", "structx2_1_1UserProfile_1_1Main.html", null ],
            [ "x2::UserProfile::Preferences", "structx2_1_1UserProfile_1_1Preferences.html", null ]
          ] ]
        ] ]
      ] ]
    ] ],
    [ "FCGX_Request", null, [
      [ "x2::Request", "structx2_1_1Request.html", null ]
    ] ],
    [ "json_object_iterator", null, [
      [ "x2::utl::JsonIt", "classx2_1_1utl_1_1JsonIt.html", null ]
    ] ],
    [ "x2::ParentOf< C >", "structx2_1_1ParentOf.html", [
      [ "x2::ChildAndParentOf< P, C, Self >", "structx2_1_1ChildAndParentOf.html", null ]
    ] ],
    [ "x2::ParentOf< Form >", "structx2_1_1ParentOf.html", [
      [ "x2::Page", "classx2_1_1Page.html", [
        [ "x2::About", "classx2_1_1About.html", null ],
        [ "x2::Activate", "classx2_1_1Activate.html", null ],
        [ "x2::adm::Base", "classx2_1_1adm_1_1Base.html", [
          [ "x2::adm::Admin", "classx2_1_1adm_1_1Admin.html", null ],
          [ "x2::adm::ClearPart", "classx2_1_1adm_1_1ClearPart.html", null ],
          [ "x2::adm::Groups", "classx2_1_1adm_1_1Groups.html", null ],
          [ "x2::adm::Part", "classx2_1_1adm_1_1Part.html", null ],
          [ "x2::adm::Sql", "classx2_1_1adm_1_1Sql.html", null ],
          [ "x2::adm::Sys", "classx2_1_1adm_1_1Sys.html", null ],
          [ "x2::adm::Xss", "classx2_1_1adm_1_1Xss.html", null ]
        ] ],
        [ "x2::Contacts", "classx2_1_1Contacts.html", null ],
        [ "x2::Home", "classx2_1_1Home.html", null ],
        [ "x2::Login", "classx2_1_1Login.html", null ],
        [ "x2::Notifications", "classx2_1_1Notifications.html", null ],
        [ "x2::ph::Base", "classx2_1_1ph_1_1Base.html", [
          [ "x2::ph::BasePost", "classx2_1_1ph_1_1BasePost.html", [
            [ "x2::ph::Post", "classx2_1_1ph_1_1Post.html", null ],
            [ "x2::ph::Topic", "classx2_1_1ph_1_1Topic.html", null ]
          ] ],
          [ "x2::ph::BbcodeData", "classx2_1_1ph_1_1BbcodeData.html", null ],
          [ "x2::ph::Close", "classx2_1_1ph_1_1Close.html", null ],
          [ "x2::ph::Delivered", "classx2_1_1ph_1_1Delivered.html", null ],
          [ "x2::ph::Part", "classx2_1_1ph_1_1Part.html", null ],
          [ "x2::ph::Phorum", "classx2_1_1ph_1_1Phorum.html", null ],
          [ "x2::ph::PrivatePart", "classx2_1_1ph_1_1PrivatePart.html", null ],
          [ "x2::ph::Remove", "classx2_1_1ph_1_1Remove.html", null ],
          [ "x2::ph::Trash", "classx2_1_1ph_1_1Trash.html", null ]
        ] ],
        [ "x2::RpcJs", "classx2_1_1RpcJs.html", null ],
        [ "x2::RpcServer", "classx2_1_1RpcServer.html", null ],
        [ "x2::Rss", "classx2_1_1Rss.html", null ],
        [ "x2::Search", "classx2_1_1Search.html", null ],
        [ "x2::Sitemap", "classx2_1_1Sitemap.html", null ],
        [ "x2::ThePage", "classx2_1_1ThePage.html", null ],
        [ "x2::UserProfile", "classx2_1_1UserProfile.html", null ]
      ] ]
    ] ],
    [ "x2::ParentOf< Input >", "structx2_1_1ParentOf.html", [
      [ "x2::ChildAndParentOf< Page, Input, Form >", "structx2_1_1ChildAndParentOf.html", null ]
    ] ],
    [ "facet", null, [
      [ "x2::MessagesCatalog", "structx2_1_1MessagesCatalog.html", null ]
    ] ],
    [ "map", null, [
      [ "x2::dta::PartAccess", "structx2_1_1dta_1_1PartAccess.html", null ]
    ] ],
    [ "runtime_error", null, [
      [ "x2::ServerError", "structx2_1_1ServerError.html", [
        [ "x2::ForbiddenError", "structx2_1_1ForbiddenError.html", null ],
        [ "x2::NotFoundError", "structx2_1_1NotFoundError.html", [
          [ "x2::AddressError", "structx2_1_1AddressError.html", null ]
        ] ]
      ] ],
      [ "x2::sql::Error", "structx2_1_1sql_1_1Error.html", [
        [ "x2::dta::UserResult::ErrorNotFound", "structx2_1_1dta_1_1UserResult_1_1ErrorNotFound.html", null ],
        [ "x2::sql::BadValueCastError", "structx2_1_1sql_1_1BadValueCastError.html", null ],
        [ "x2::sql::ConnectionError", "structx2_1_1sql_1_1ConnectionError.html", null ],
        [ "x2::sql::ErrorIntegrityConstraintViolation", "structx2_1_1sql_1_1ErrorIntegrityConstraintViolation.html", null ],
        [ "x2::sql::ErrorLockNotAvailable", "structx2_1_1sql_1_1ErrorLockNotAvailable.html", null ]
      ] ]
    ] ],
    [ "set", null, [
      [ "x2::dta::Users2rise", "structx2_1_1dta_1_1Users2rise.html", null ]
    ] ],
    [ "timeval", null, [
      [ "x2::Timer", "structx2_1_1Timer.html", null ]
    ] ],
    [ "x2::Archive", "classx2_1_1Archive.html", null ],
    [ "x2::bbcode::BbCodable", "classx2_1_1bbcode_1_1BbCodable.html", [
      [ "x2::ph::BbCodable", "structx2_1_1ph_1_1BbCodable.html", null ]
    ] ],
    [ "x2::bbcode::BbCodeInfo", "structx2_1_1bbcode_1_1BbCodeInfo.html", null ],
    [ "x2::bbcode::Descriptor", "structx2_1_1bbcode_1_1Descriptor.html", null ],
    [ "x2::bbcode::Smile", "structx2_1_1bbcode_1_1Smile.html", null ],
    [ "x2::Cache", "classx2_1_1Cache.html", null ],
    [ "x2::Cfg", "classx2_1_1Cfg.html", null ],
    [ "x2::Cfg::Activity", "structx2_1_1Cfg_1_1Activity.html", null ],
    [ "x2::Cfg::Admin", "structx2_1_1Cfg_1_1Admin.html", null ],
    [ "x2::Cfg::Admin::Policy", "structx2_1_1Cfg_1_1Admin_1_1Policy.html", null ],
    [ "x2::Cfg::Admin::Policy::NewUser", "structx2_1_1Cfg_1_1Admin_1_1Policy_1_1NewUser.html", null ],
    [ "x2::Cfg::Admin::Sql", "structx2_1_1Cfg_1_1Admin_1_1Sql.html", null ],
    [ "x2::Cfg::Avatar", "structx2_1_1Cfg_1_1Avatar.html", null ],
    [ "x2::Cfg::Cache", "structx2_1_1Cfg_1_1Cache.html", null ],
    [ "x2::Cfg::Cache::Fill", "structx2_1_1Cfg_1_1Cache_1_1Fill.html", null ],
    [ "x2::Cfg::Cache::Fill::Try", "structx2_1_1Cfg_1_1Cache_1_1Fill_1_1Try.html", null ],
    [ "x2::Cfg::Client", "structx2_1_1Cfg_1_1Client.html", null ],
    [ "x2::Cfg::Cookie", "structx2_1_1Cfg_1_1Cookie.html", null ],
    [ "x2::Cfg::LastPosts", "structx2_1_1Cfg_1_1LastPosts.html", null ],
    [ "x2::Cfg::Localization", "structx2_1_1Cfg_1_1Localization.html", null ],
    [ "x2::Cfg::Logging", "structx2_1_1Cfg_1_1Logging.html", null ],
    [ "x2::Cfg::Message", "structx2_1_1Cfg_1_1Message.html", null ],
    [ "x2::Cfg::Message::MaxSize", "structx2_1_1Cfg_1_1Message_1_1MaxSize.html", null ],
    [ "x2::Cfg::Postgres", "structx2_1_1Cfg_1_1Postgres.html", null ],
    [ "x2::Cfg::Server", "structx2_1_1Cfg_1_1Server.html", null ],
    [ "x2::Cfg::Smtp", "structx2_1_1Cfg_1_1Smtp.html", null ],
    [ "x2::Cfg::Table", "structx2_1_1Cfg_1_1Table.html", null ],
    [ "x2::Cfg::Themes", "structx2_1_1Cfg_1_1Themes.html", null ],
    [ "x2::Cfg::Themes::Theme", "structx2_1_1Cfg_1_1Themes_1_1Theme.html", null ],
    [ "x2::Cfg::Xss", "structx2_1_1Cfg_1_1Xss.html", null ],
    [ "x2::ChildOf< Parent, Self >", "structx2_1_1ChildOf.html", null ],
    [ "x2::Cookie", "classx2_1_1Cookie.html", null ],
    [ "x2::Dom", "structx2_1_1Dom.html", [
      [ "x2::Img", "structx2_1_1Img.html", null ],
      [ "x2::Input", "structx2_1_1Input.html", null ],
      [ "x2::Label", "structx2_1_1Label.html", null ],
      [ "x2::Link", "structx2_1_1Link.html", null ]
    ] ],
    [ "x2::dta::adm::PartAccess::setCompare", "structx2_1_1dta_1_1adm_1_1PartAccess_1_1setCompare.html", null ],
    [ "x2::LastModified", "classx2_1_1LastModified.html", null ],
    [ "x2::mail::Smtp", "classx2_1_1mail_1_1Smtp.html", null ],
    [ "x2::MenuItem", "structx2_1_1MenuItem.html", null ],
    [ "x2::MenuItem::IdComp", "structx2_1_1MenuItem_1_1IdComp.html", null ],
    [ "x2::Navigation", "structx2_1_1Navigation.html", null ],
    [ "x2::Page::Dispatcher", "classx2_1_1Page_1_1Dispatcher.html", null ],
    [ "x2::ParentOf< Child >", "structx2_1_1ParentOf.html", null ],
    [ "x2::ph::Headers", "structx2_1_1ph_1_1Headers.html", null ],
    [ "x2::ph::Headers::D", "structx2_1_1ph_1_1Headers_1_1D.html", null ],
    [ "x2::ph::Headers::T", "structx2_1_1ph_1_1Headers_1_1T.html", null ],
    [ "x2::ph::Removing", "structx2_1_1ph_1_1Removing.html", null ],
    [ "x2::ph::Template", "structx2_1_1ph_1_1Template.html", null ],
    [ "x2::Request::File", "structx2_1_1Request_1_1File.html", null ],
    [ "x2::Response", "classx2_1_1Response.html", null ],
    [ "x2::Select::Item", "structx2_1_1Select_1_1Item.html", null ],
    [ "x2::Serializable", "classx2_1_1Serializable.html", [
      [ "x2::dta::Rights", "structx2_1_1dta_1_1Rights.html", [
        [ "x2::dta::adm::PartAccess", "structx2_1_1dta_1_1adm_1_1PartAccess.html", null ]
      ] ],
      [ "x2::sql::CachedResult", "classx2_1_1sql_1_1CachedResult.html", [
        [ "x2::dta::BaseUserResult< sql::CachedResult, UserResult >", "structx2_1_1dta_1_1BaseUserResult.html", [
          [ "x2::dta::UserResult", "classx2_1_1dta_1_1UserResult.html", null ]
        ] ],
        [ "x2::dta::adm::GroupsResult", "classx2_1_1dta_1_1adm_1_1GroupsResult.html", null ],
        [ "x2::dta::GroupsOfUserResult", "classx2_1_1dta_1_1GroupsOfUserResult.html", null ],
        [ "x2::dta::ph::LastPostsResult", "structx2_1_1dta_1_1ph_1_1LastPostsResult.html", null ],
        [ "x2::sql::ComplexCachedResult", "classx2_1_1sql_1_1ComplexCachedResult.html", [
          [ "x2::dta::BaseUserResult< sql::ComplexCachedResult, ComplexUserResult >", "structx2_1_1dta_1_1BaseUserResult.html", [
            [ "x2::dta::ComplexUserResult", "classx2_1_1dta_1_1ComplexUserResult.html", null ]
          ] ],
          [ "x2::dta::ph::PartsResult", "structx2_1_1dta_1_1ph_1_1PartsResult.html", [
            [ "x2::dta::adm::PartsResult", "structx2_1_1dta_1_1adm_1_1PartsResult.html", null ]
          ] ],
          [ "x2::dta::ph::PostsResult", "structx2_1_1dta_1_1ph_1_1PostsResult.html", null ],
          [ "x2::dta::ph::TopicsResult", "structx2_1_1dta_1_1ph_1_1TopicsResult.html", null ]
        ] ]
      ] ]
    ] ],
    [ "x2::sql::Iterator< T >", "classx2_1_1sql_1_1Iterator.html", null ],
    [ "x2::sql::noncopyable", "classx2_1_1sql_1_1noncopyable.html", [
      [ "x2::sql::Session", "classx2_1_1sql_1_1Session.html", [
        [ "x2::sql::PhSession", "classx2_1_1sql_1_1PhSession.html", null ]
      ] ],
      [ "x2::sql::Transaction", "classx2_1_1sql_1_1Transaction.html", null ]
    ] ],
    [ "x2::sql::Result", "classx2_1_1sql_1_1Result.html", [
      [ "x2::dta::NotificationsResult", "classx2_1_1dta_1_1NotificationsResult.html", null ],
      [ "x2::sql::CachedResult", "classx2_1_1sql_1_1CachedResult.html", null ],
      [ "x2::sql::Statement", "classx2_1_1sql_1_1Statement.html", null ]
    ] ],
    [ "x2::sql::Result::Current", "structx2_1_1sql_1_1Result_1_1Current.html", null ],
    [ "x2::sql::Statement::Parameter", "classx2_1_1sql_1_1Statement_1_1Parameter.html", null ],
    [ "x2::Thread", "classx2_1_1Thread.html", null ],
    [ "x2::Url", "structx2_1_1Url.html", null ],
    [ "x2::User", "structx2_1_1User.html", null ],
    [ "x2::UserProfile::Main::Banned::Values", "structx2_1_1UserProfile_1_1Main_1_1Banned_1_1Values.html", null ],
    [ "x2::utl::F", "classx2_1_1utl_1_1F.html", [
      [ "x2::utl::Pf", "classx2_1_1utl_1_1Pf.html", null ]
    ] ],
    [ "x2::utl::Json", "classx2_1_1utl_1_1Json.html", null ],
    [ "x2::utl::JsonIt::Pair", "structx2_1_1utl_1_1JsonIt_1_1Pair.html", null ],
    [ "x2::utl::SelfName", "classx2_1_1utl_1_1SelfName.html", null ],
    [ "x2::utl::T2s", "structx2_1_1utl_1_1T2s.html", null ],
    [ "x2::utl::TimeT", "structx2_1_1utl_1_1TimeT.html", null ],
    [ "x2::Visited", "classx2_1_1Visited.html", null ],
    [ "x2::Visited::Cache< T >", "structx2_1_1Visited_1_1Cache.html", null ],
    [ "x2::Visited::Cache< T >::Stat", "structx2_1_1Visited_1_1Cache_1_1Stat.html", null ],
    [ "x2::Visited::CacheElement", "structx2_1_1Visited_1_1CacheElement.html", [
      [ "x2::Visited::Topic", "structx2_1_1Visited_1_1Topic.html", null ],
      [ "x2::Visited::User", "structx2_1_1Visited_1_1User.html", null ]
    ] ],
    [ "x2::xss::Filter", "structx2_1_1xss_1_1Filter.html", null ],
    [ "B", null, [
      [ "x2::dta::BaseUserResult< B, O >", "structx2_1_1dta_1_1BaseUserResult.html", null ]
    ] ]
];