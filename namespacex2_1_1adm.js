var namespacex2_1_1adm =
[
    [ "Admin", "classx2_1_1adm_1_1Admin.html", "classx2_1_1adm_1_1Admin" ],
    [ "Base", "classx2_1_1adm_1_1Base.html", "classx2_1_1adm_1_1Base" ],
    [ "ClearPart", "classx2_1_1adm_1_1ClearPart.html", "classx2_1_1adm_1_1ClearPart" ],
    [ "Groups", "classx2_1_1adm_1_1Groups.html", "classx2_1_1adm_1_1Groups" ],
    [ "Part", "classx2_1_1adm_1_1Part.html", "classx2_1_1adm_1_1Part" ],
    [ "Sql", "classx2_1_1adm_1_1Sql.html", "classx2_1_1adm_1_1Sql" ],
    [ "Sys", "classx2_1_1adm_1_1Sys.html", "classx2_1_1adm_1_1Sys" ],
    [ "Xss", "classx2_1_1adm_1_1Xss.html", "classx2_1_1adm_1_1Xss" ]
];