var classx2_1_1Visited =
[
    [ "Cache", "structx2_1_1Visited_1_1Cache.html", "structx2_1_1Visited_1_1Cache" ],
    [ "CacheElement", "structx2_1_1Visited_1_1CacheElement.html", "structx2_1_1Visited_1_1CacheElement" ],
    [ "Topic", "structx2_1_1Visited_1_1Topic.html", "structx2_1_1Visited_1_1Topic" ],
    [ "User", "structx2_1_1Visited_1_1User.html", "structx2_1_1Visited_1_1User" ],
    [ "Activities", "classx2_1_1Visited.html#ab908f5a135730b07bd69d123d6f29b81", null ],
    [ "Visits", "classx2_1_1Visited.html#adf219a3cc9188a9f82745bfd5072986f", null ],
    [ "CacheState", "classx2_1_1Visited.html#ac795e1b53a214a12b6e8268665c5c0e1", [
      [ "clearState", "classx2_1_1Visited.html#ac795e1b53a214a12b6e8268665c5c0e1a5e608d6608e873f59f447cfa507f12e5", null ],
      [ "dirtyState", "classx2_1_1Visited.html#ac795e1b53a214a12b6e8268665c5c0e1a950f76dec44e49ace4b935f2cf00ecae", null ]
    ] ],
    [ "clear", "classx2_1_1Visited.html#a0a07e6455d8c6f54eae9db314dda306e", null ],
    [ "get", "classx2_1_1Visited.html#a2088cc1dc52473f7791887967fbbf7fe", null ],
    [ "get", "classx2_1_1Visited.html#ab7eb87149351901858b59ce302229d8f", null ],
    [ "idle", "classx2_1_1Visited.html#aff6d31efeb38a7eb5b19976c597bd03c", null ],
    [ "put", "classx2_1_1Visited.html#a8047f75ebfaf590aafceed009c184fa8", null ],
    [ "topicCache", "classx2_1_1Visited.html#a32fa182c449eb8b2808083ca3193fed1", null ],
    [ "userCache", "classx2_1_1Visited.html#aadb36e62d3903f4986a0eefc99020eec", null ]
];