var classx2_1_1dta_1_1GroupsOfUserResult =
[
    [ "Iterator", "classx2_1_1dta_1_1GroupsOfUserResult.html#a1d7f8b75773130ea66d64ed82a05c2ab", null ],
    [ "GroupsOfUserResult", "classx2_1_1dta_1_1GroupsOfUserResult.html#aae93766ad246c20180d31ea8e7bc1ae0", null ],
    [ "before", "classx2_1_1dta_1_1GroupsOfUserResult.html#a4a26c97db03da54ad1a0e8ef7a9f988d", null ],
    [ "begin", "classx2_1_1dta_1_1GroupsOfUserResult.html#adfe0953657e8ebdae8a8637529a3afa0", null ],
    [ "belongs", "classx2_1_1dta_1_1GroupsOfUserResult.html#a794f04f07c206fb67b6ea9b7ebc7037c", null ],
    [ "description", "classx2_1_1dta_1_1GroupsOfUserResult.html#a765a6099a4cd16c1a1c64ef53b995f12", null ],
    [ "end", "classx2_1_1dta_1_1GroupsOfUserResult.html#a7ab5d7ba3eb61f05766517a48ddccab2", null ],
    [ "id", "classx2_1_1dta_1_1GroupsOfUserResult.html#aca5eb52f955f7980586bf3b2d4f27045", null ],
    [ "init", "classx2_1_1dta_1_1GroupsOfUserResult.html#ac943a8a8fc94c9f8fdc764b9e9d63596", null ],
    [ "modified", "classx2_1_1dta_1_1GroupsOfUserResult.html#acbe32bf2d673af58ccf5406f769bdba0", null ],
    [ "name", "classx2_1_1dta_1_1GroupsOfUserResult.html#ac8a467b339c0f8428fef3eeefa6fccd2", null ]
];