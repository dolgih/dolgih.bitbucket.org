var structx2_1_1adm_1_1Part_1_1EditForm =
[
    [ "EditForm", "structx2_1_1adm_1_1Part_1_1EditForm.html#aff774e6730c5ded67e3eb43e4008026e", null ],
    [ "accessList", "structx2_1_1adm_1_1Part_1_1EditForm.html#a6312555681eff64e4f252e2b3a56f4b6", null ],
    [ "author", "structx2_1_1adm_1_1Part_1_1EditForm.html#ab1f5a76cc8f83dbedd467540eb756940", null ],
    [ "banGroup", "structx2_1_1adm_1_1Part_1_1EditForm.html#ab2df9d04c24a4c004c0b41c23a12b645", null ],
    [ "clear", "structx2_1_1adm_1_1Part_1_1EditForm.html#a7d46a51cfc991c31230fbffaaf6041dd", null ],
    [ "description", "structx2_1_1adm_1_1Part_1_1EditForm.html#a3ee444a187a4901a9cb951ae5338aae9", null ],
    [ "lAccessList", "structx2_1_1adm_1_1Part_1_1EditForm.html#a0d929d8a2796fed3840124bf35dd3d8c", null ],
    [ "lAuthor", "structx2_1_1adm_1_1Part_1_1EditForm.html#a9ce06fca7b9fb0ab304a77444e9c2b29", null ],
    [ "lBanGroup", "structx2_1_1adm_1_1Part_1_1EditForm.html#a0c4a3c8c31639251f75f29ec670ef790", null ],
    [ "lDescription", "structx2_1_1adm_1_1Part_1_1EditForm.html#a99cba467f2980074c8e4ff2ac5de22d8", null ],
    [ "lName", "structx2_1_1adm_1_1Part_1_1EditForm.html#a74b678e9514ad8a4d2c8dd0e8bd3e941", null ],
    [ "lWeight", "structx2_1_1adm_1_1Part_1_1EditForm.html#a5fc1e15736ffdca1275d338094a2985d", null ],
    [ "name", "structx2_1_1adm_1_1Part_1_1EditForm.html#ac4b24765a97a261ec36d8b82b92dc46b", null ],
    [ "remove", "structx2_1_1adm_1_1Part_1_1EditForm.html#aba610a1632fed23c19a7316e01084f28", null ],
    [ "submit", "structx2_1_1adm_1_1Part_1_1EditForm.html#a63c528465336af98e5981d68cfcbaaa6", null ],
    [ "weight", "structx2_1_1adm_1_1Part_1_1EditForm.html#ac48cce40ba2b722551bb816742b5360d", null ]
];