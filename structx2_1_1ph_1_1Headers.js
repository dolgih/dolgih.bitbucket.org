var structx2_1_1ph_1_1Headers =
[
    [ "D", "structx2_1_1ph_1_1Headers_1_1D.html", "structx2_1_1ph_1_1Headers_1_1D" ],
    [ "T", "structx2_1_1ph_1_1Headers_1_1T.html", "structx2_1_1ph_1_1Headers_1_1T" ],
    [ "init", "structx2_1_1ph_1_1Headers.html#a1f9f3ddeebe0accbf7d21f9e0b1777ee", null ],
    [ "partDescr", "structx2_1_1ph_1_1Headers.html#a6d3fc4fba44c4bdf968bcd5afa509760", null ],
    [ "partTitle", "structx2_1_1ph_1_1Headers.html#a5180ac0060c9e48d2b63e44ec5ce5dab", null ],
    [ "postDescr", "structx2_1_1ph_1_1Headers.html#a7edba6cff3d282a7240ca8b662b7cc78", null ],
    [ "postTitle", "structx2_1_1ph_1_1Headers.html#a8aab00039f96e6b5961543b5c4392a8c", null ],
    [ "topicDescr", "structx2_1_1ph_1_1Headers.html#ac9193d6960e0d0c21f08df0a73c0ca9e", null ],
    [ "topicTitle", "structx2_1_1ph_1_1Headers.html#af1d829c2cc08d14ff24e06fe95b366e8", null ],
    [ "d", "structx2_1_1ph_1_1Headers.html#a8772194cad31add23485046b0fdca72d", null ],
    [ "isPages", "structx2_1_1ph_1_1Headers.html#a3a9389aff6d1b52f638689b53946af16", null ],
    [ "t", "structx2_1_1ph_1_1Headers.html#a5f4a6f159e653abdc74a0b76eeef7891", null ]
];