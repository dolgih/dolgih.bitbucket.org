var classx2_1_1Cookie =
[
    [ "Cookie", "classx2_1_1Cookie.html#afce6020429f4c23e609dc5d3c702c207", null ],
    [ "clear", "classx2_1_1Cookie.html#ac729f58e89b135887fc4f04c76f886a9", null ],
    [ "getCsrf", "classx2_1_1Cookie.html#a4236a58603229fd6d4b89f6541eeb572", null ],
    [ "getCsrfUS", "classx2_1_1Cookie.html#a2f176ced1e7a537ed2d93b15c57bb038", null ],
    [ "getId", "classx2_1_1Cookie.html#a6ac286bce8dbe96f91ff017cf9c7397b", null ],
    [ "getLocale", "classx2_1_1Cookie.html#a6fc64ac8bcd8035698274d0cfd4cb942", null ],
    [ "set", "classx2_1_1Cookie.html#a8fb3c63bcfc8014e6b61d7cfe89054e1", null ],
    [ "set", "classx2_1_1Cookie.html#a6c4e4679d95f6584fd8f81d6829e5c8a", null ],
    [ "setCsrf", "classx2_1_1Cookie.html#a2081f6de620d34e9d5032f39bf652e8d", null ],
    [ "changed", "classx2_1_1Cookie.html#aa7b38c2fd5736a2fc2362eff366098b6", null ],
    [ "csrf", "classx2_1_1Cookie.html#a234ec19d9b9adfaab0658378b51219f5", null ],
    [ "id", "classx2_1_1Cookie.html#aa4d24c5706852e3143a8accbe11b7d7d", null ],
    [ "locale", "classx2_1_1Cookie.html#ac647168dee1e31f6a4f43bdef52ba0ea", null ],
    [ "request", "classx2_1_1Cookie.html#a05ff142fe70458365b1124ff757a64d3", null ],
    [ "response", "classx2_1_1Cookie.html#a3e42cc1848b9e327c35055c71eaa855e", null ]
];