var searchData=
[
  ['d',['d',['../structx2_1_1Visited_1_1Cache_1_1Stat.html#ac1c874fb2a196f75faa4b74784d4696e',1,'x2::Visited::Cache::Stat::d()'],['../structx2_1_1ph_1_1Headers.html#a8772194cad31add23485046b0fdca72d',1,'x2::ph::Headers::d()']]],
  ['data',['data',['../classx2_1_1Archive.html#afa15351105a801471a4db112c1dde076',1,'x2::Archive']]],
  ['database',['database',['../structx2_1_1Cfg_1_1Postgres.html#a5eb23ead06b0ff93bbd95781e9b9353f',1,'x2::Cfg::Postgres']]],
  ['date1',['date1',['../structx2_1_1Search_1_1TheForm.html#a93ff0606e743f466230bf5e2d81229f8',1,'x2::Search::TheForm']]],
  ['date2',['date2',['../structx2_1_1Search_1_1TheForm.html#a06f11ec7813d6aeede9362085f30c223',1,'x2::Search::TheForm']]],
  ['defaultlocale',['defaultLocale',['../structx2_1_1Cfg_1_1Localization.html#ad1f3a0eeebd629365a0dd3de23f9635f',1,'x2::Cfg::Localization']]],
  ['defaulttheme',['defaultTheme',['../structx2_1_1Cfg_1_1Themes.html#a159f29d8da6bed74f9df080f3a6f72e2',1,'x2::Cfg::Themes']]],
  ['deftsconfig',['defTSConfig',['../structx2_1_1Cfg_1_1Postgres.html#ada13b16ddc4552f5672f44dce34f2487',1,'x2::Cfg::Postgres']]],
  ['deny',['deny',['../structx2_1_1Cfg_1_1Xss.html#a8b0395aefda3e9cd5d582e453b1f328c',1,'x2::Cfg::Xss']]],
  ['description',['description',['../classx2_1_1Cfg.html#abf49854f0bf54191f15af1f7f8b7fce1',1,'x2::Cfg::description()'],['../structx2_1_1UserProfile_1_1Main_1_1Banned_1_1Values.html#a2e8cb4743f920ac60bb84dce6b8bbc52',1,'x2::UserProfile::Main::Banned::Values::description()'],['../structx2_1_1UserProfile_1_1Main_1_1Banned.html#aec690cbb561f159654594d6811474f58',1,'x2::UserProfile::Main::Banned::description()'],['../structx2_1_1adm_1_1Part_1_1EditForm.html#a3ee444a187a4901a9cb951ae5338aae9',1,'x2::adm::Part::EditForm::description()']]],
  ['disabled',['disabled',['../classx2_1_1sql_1_1Transaction.html#a6a1b1a533e6ed7010385e8444afb9f60',1,'x2::sql::Transaction']]],
  ['dispatcher',['dispatcher',['../classx2_1_1Page.html#a9762ac78a82a0ca93a3173e51435aeb7',1,'x2::Page::dispatcher()'],['../classx2_1_1adm_1_1Base.html#a44672f81f01efbf1967511b75f8a4459',1,'x2::adm::Base::dispatcher()'],['../classx2_1_1ph_1_1Base.html#ac5bd046d2de8cb22e5b75d736e39a43b',1,'x2::ph::Base::dispatcher()']]],
  ['docroot',['docRoot',['../classx2_1_1Cfg.html#a8e96792eca5d3987420a58b8a0b2aa9e',1,'x2::Cfg']]],
  ['domain',['domain',['../structx2_1_1Cfg_1_1Localization.html#a14baad109dafcaef6589f0393ac2d543',1,'x2::Cfg::Localization']]],
  ['done',['done',['../classx2_1_1sql_1_1Transaction.html#aa728907ebc3355c7ce2099494b20116f',1,'x2::sql::Transaction']]]
];
