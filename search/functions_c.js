var searchData=
[
  ['m',['M',['../structx2_1_1dta_1_1Rights.html#a45c44d04a71358bdc78530d9c6d67118',1,'x2::dta::Rights']]],
  ['mail',['mail',['../classx2_1_1Page.html#a71208e752c9c702698533688405295c7',1,'x2::Page']]],
  ['mail4activate',['mail4activate',['../classx2_1_1UserProfile.html#a36b095c37c78367296a174c9a92f3aff',1,'x2::UserProfile']]],
  ['mail4login',['mail4login',['../classx2_1_1Login.html#a12d98c8ae3a5aa49e09629fbc4d5ca5e',1,'x2::Login']]],
  ['main',['Main',['../structx2_1_1UserProfile_1_1Main.html#a2eeafa55f93424e6cf4c76a63450b3d2',1,'x2::UserProfile::Main::Main()'],['../x2_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main(int argc, char **argv):&#160;x2.cpp'],['../postgres_2test_8cpp.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;test.cpp'],['../bbcode_2test_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main(int argc, char **argv):&#160;test.cpp'],['../mail_2test_8cpp.html#a840291bc02cba5474a4cb46a9b9566fe',1,'main(void):&#160;test.cpp']]],
  ['makeempty',['makeEmpty',['../classx2_1_1sql_1_1Result.html#a58231c72c43458b5c32195dc4e55d8e5',1,'x2::sql::Result']]],
  ['makekey',['makeKey',['../structx2_1_1Url.html#ad998da936000210dbfdbb943d0818302',1,'x2::Url']]],
  ['markdown',['markdown',['../classx2_1_1Page.html#a371c68122790693991ad80728189333d',1,'x2::Page']]],
  ['maxelements',['maxElements',['../structx2_1_1Visited_1_1Topic.html#a54744efc83c9ca978281b9ce18b41d71',1,'x2::Visited::Topic::maxElements()'],['../structx2_1_1Visited_1_1User.html#a895c7ab6e1d19fe107a83d1266c7233c',1,'x2::Visited::User::maxElements()']]],
  ['maxpage',['maxPage',['../classx2_1_1dta_1_1ComplexUserResult.html#ad71cf5633673d03fcef771bff451a4c7',1,'x2::dta::ComplexUserResult']]],
  ['maxpostspage',['maxPostsPage',['../structx2_1_1dta_1_1ph_1_1TopicsResult.html#a7b7e0ec2b5da6f044af2f0c315fd741f',1,'x2::dta::ph::TopicsResult']]],
  ['maxtopicspage',['maxTopicsPage',['../structx2_1_1dta_1_1ph_1_1PartsResult.html#a1c2a163c64b1380c34b3b0a8ce4e1f92',1,'x2::dta::ph::PartsResult']]],
  ['menuitem',['MenuItem',['../structx2_1_1MenuItem.html#aa49dd4d75660de3ca68b213fcdb05de9',1,'x2::MenuItem::MenuItem(int id, int properties=Separator)'],['../structx2_1_1MenuItem.html#a276990ed3dbced86425ad1ed7c4fc9b3',1,'x2::MenuItem::MenuItem(int id, Str name, Str link, Str title, int properties=0)'],['../structx2_1_1MenuItem.html#a343047bd5279be44ce5c9ad1d85d3910',1,'x2::MenuItem::MenuItem(int id, Str name, Menu &amp;subMenu, Str title=Str(), int properties=0)']]],
  ['messageidline',['messageIdLine',['../classx2_1_1mail_1_1Smtp.html#a28727138c00305cac3eefd51f7d48837',1,'x2::mail::Smtp']]],
  ['method',['method',['../structx2_1_1Request.html#af6d96e4162f23847641b57e9c7c037f0',1,'x2::Request']]],
  ['minelements',['minElements',['../structx2_1_1Visited_1_1Topic.html#a5300b59bd44deb06867adb67d767fffb',1,'x2::Visited::Topic::minElements()'],['../structx2_1_1Visited_1_1User.html#a5d5a9500af4c8ee268f415120eeae71a',1,'x2::Visited::User::minElements()']]],
  ['mkparam',['mkParam',['../structx2_1_1Url.html#a33a275a8d6a1869ed4875b918ae7c475',1,'x2::Url::mkParam()'],['../structx2_1_1Url.html#ae2e37ab68a61f3e2bb1c8a4f8b2b0538',1,'x2::Url::mkParam(const T &amp;param, const Ts &amp;...params)']]],
  ['modified',['modified',['../structx2_1_1dta_1_1BaseUserResult.html#aab8f55361e08b03c401713c2726d4a12',1,'x2::dta::BaseUserResult::modified()'],['../classx2_1_1dta_1_1GroupsOfUserResult.html#acbe32bf2d673af58ccf5406f769bdba0',1,'x2::dta::GroupsOfUserResult::modified()'],['../classx2_1_1dta_1_1adm_1_1GroupsResult.html#afc8c5fd2a3988848ee64b271abe36fea',1,'x2::dta::adm::GroupsResult::modified()'],['../structx2_1_1dta_1_1ph_1_1PartsResult.html#afba47b950e47112752b0471bda79380a',1,'x2::dta::ph::PartsResult::modified()'],['../structx2_1_1dta_1_1ph_1_1TopicsResult.html#aa607daf2a8615fe41a5cb14fbcc6d1af',1,'x2::dta::ph::TopicsResult::modified()'],['../structx2_1_1dta_1_1ph_1_1PostsResult.html#a572f4de4539233f2de6c1393975daf75',1,'x2::dta::ph::PostsResult::modified()']]],
  ['msgbox',['msgBox',['../classx2_1_1Page.html#aab656b1b6b293ed5d39bb42e8ef37d43',1,'x2::Page']]],
  ['multicheckbox',['MultiCheckBox',['../structx2_1_1MultiCheckBox.html#af6d6df27b3e97f3b42dd66380d9c507b',1,'x2::MultiCheckBox']]]
];
