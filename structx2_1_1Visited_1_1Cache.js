var structx2_1_1Visited_1_1Cache =
[
    [ "Stat", "structx2_1_1Visited_1_1Cache_1_1Stat.html", "structx2_1_1Visited_1_1Cache_1_1Stat" ],
    [ "Index", "structx2_1_1Visited_1_1Cache.html#ad57b9157804ca749ece49ddc1437e402", null ],
    [ "List", "structx2_1_1Visited_1_1Cache.html#a393ea5f0953db7600be87fae5e2807ae", null ],
    [ "Cache", "structx2_1_1Visited_1_1Cache.html#a9221f32e0b7327899d242c6b75cd5c1f", null ],
    [ "get", "structx2_1_1Visited_1_1Cache.html#a579f11b424d55921b286fcfcdf953789", null ],
    [ "idle", "structx2_1_1Visited_1_1Cache.html#a622e8ab7fe6cc68e76961f2b09eacb4e", null ],
    [ "insert", "structx2_1_1Visited_1_1Cache.html#a1ff8babde4919f536ceb9f8a4613d5bb", null ],
    [ "splice", "structx2_1_1Visited_1_1Cache.html#a71c15246f9184228e927ad6dbec0d743", null ],
    [ "write", "structx2_1_1Visited_1_1Cache.html#a8cdb5d22f320c63107e8db07ba9ea8b0", null ],
    [ "guard", "structx2_1_1Visited_1_1Cache.html#a4cb7885a6aa9729654d20ba343393221", null ],
    [ "index", "structx2_1_1Visited_1_1Cache.html#aa4005905ab3e8adfa9375c65900e7378", null ],
    [ "lastWriteTime", "structx2_1_1Visited_1_1Cache.html#a17824db61271ff9959e71eb1dcf1a78f", null ],
    [ "list", "structx2_1_1Visited_1_1Cache.html#aaff730e52f52968ba39dedf2f96d404b", null ],
    [ "modifiedCount", "structx2_1_1Visited_1_1Cache.html#a6f2bf4419619f794e4d0fbafb5edba38", null ],
    [ "stat", "structx2_1_1Visited_1_1Cache.html#a76c7660bc800f5bc57307f0e78fd7d1e", null ]
];