var dir_472fafaf0f039b9e6b043966c1ef7dbf =
[
    [ "adm", "dir_bbdf460b511f1a75828f080403d7efbf.html", "dir_bbdf460b511f1a75828f080403d7efbf" ],
    [ "ph", "dir_64403ee636bdc2073320c082dce5c955.html", "dir_64403ee636bdc2073320c082dce5c955" ],
    [ "base.cpp", "data_2base_8cpp.html", null ],
    [ "base.h", "base_8h.html", [
      [ "Rights", "structx2_1_1dta_1_1Rights.html", "structx2_1_1dta_1_1Rights" ],
      [ "PartAccess", "structx2_1_1dta_1_1PartAccess.html", "structx2_1_1dta_1_1PartAccess" ]
    ] ],
    [ "notifications.cpp", "data_2notifications_8cpp.html", null ],
    [ "notifications.h", "notifications_8h.html", [
      [ "NotificationsResult", "classx2_1_1dta_1_1NotificationsResult.html", "classx2_1_1dta_1_1NotificationsResult" ]
    ] ],
    [ "user.cpp", "data_2user_8cpp.html", null ],
    [ "user.h", "data_2user_8h.html", "data_2user_8h" ]
];